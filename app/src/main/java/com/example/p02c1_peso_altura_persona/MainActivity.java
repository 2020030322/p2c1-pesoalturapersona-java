package com.example.p02c1_peso_altura_persona;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblResultado;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);
        lblResultado = findViewById(R.id.lblResultado);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                double ALTURA, PESO, IMC;
                if (txtAltura.getText().toString().matches("") || txtPeso.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Favor de ingresar el Peso y/o la Altura", Toast.LENGTH_SHORT).show();
                } else {
                    ALTURA = Double.parseDouble(txtAltura.getText().toString());
                    PESO = Double.parseDouble(txtPeso.getText().toString()); // Corrección aquí
                    IMC = PESO / (ALTURA * ALTURA);
                    lblResultado.setText("El IMC es: " + String.format("%.2f", IMC) + " :o"); // Corrección aquí (formato de %.2f)
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblResultado.setText(".... --- .-.. .-  / .-- . -. .- ... ");
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { finish(); }
        });
    }
}